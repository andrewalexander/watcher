package main

import (
	"context"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/fsnotify/fsnotify"
)

func main() {
	// set up our context and signal handler - this allows us to ctrl+c as well
	// as gives us a way to exit if we encounter a fatal error (or we could
	// handle things and resume a loop, as in the case of an http server). this
	// is a bounded channel look up the differences between channels with
	// lengths (bounded) and without (unbounded):
	// https://github.com/ardanlabs/gotraining/blob/master/topics/go/concurrency/channels/README.md
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	// if we call cancel(), anything that checks for ctx.Done() will see it and
	// allow us to terminate a loop when selecting from it
	ctx, cancel := context.WithCancel(context.Background())

	// set up watcher
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatalf("Could not set up watcher, %s", err)
	}
	defer watcher.Close()

	// set up our processor to process queries as they come in; spawn an
	// anonymous goroutine that accepts a context. we could also throw go in
	// front of any other function invocation to spawn it as a goroutine.
	go func(ctx context.Context) {
		// label our for loop - necessary because a break in the case breaks
		// the case statement, not the for loop. will result in an infinite
		// print loop without this.
	L:
		for {
			select {
			// we got an event from our file watcher
			case event, ok := <-watcher.Events:
				if !ok {
					log.Println(errors.New("watcher failed")) // creates an `error` type; useful for other things that expect error types
				}
				//log.Println("event:", event)
				if event.Op&fsnotify.Write == fsnotify.Write || event.Op&fsnotify.Rename == fsnotify.Rename {
					content, err := ioutil.ReadFile(event.Name)
					if err != nil {
						log.Printf("error reading %s, %s", event.Name, err)
						continue
					}
					// content is a []byte. you can convert to string or use it as []byte in most cases
					log.Printf("users.txt updated. updated contents:\n%s", string(content))
					// for some reason we need to do this otherwise we only get one event and that's it
					watcher.Add(event.Name)
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					err = errors.New("watcher failed")
					log.Println(err)
					// if we wanted to, we could also write to done to exit the program on this "fatal error"
					// done <- syscall.SIGTERM
				}
				// fun example given context but this is the "happy path"
				log.Println("error:", err)
			case <-ctx.Done():
				// this is where we could have a lot of cleanup logic to exit
				// gracefully after receiving the cancel signal
				log.Println("context canceled. exiting watcher loop")
				break L
			}
		}
		// we abuse done; assuming we had our context canceled, report back up
		// the chain that we are done cleaning up
		done <- syscall.SIGTERM
	}(ctx) // here we are passing our context into the anonymous function; like calling func_name(parameter)

	// add our file to the watcher: when the file changes, send the results to our processor
	if err := watcher.Add("users.txt"); err != nil {
		log.Fatalf("failed adding users.txt to watcher, %s", err)
	}

	// receive in a continuous loop until one of our select conditions are
	// true - this is the main place to control the flow of our program
	ticker := time.Tick(1 * time.Minute)
	uptime := 0
	for {
		select {
		// example of using time.Ticker for timeouts - in this case we just
		// print a "still here" but this concept can normally be used to
		// time out a connection or to flush a buffer
		case <-ticker:
			uptime++
			log.Printf("alive for %d minutes\n", uptime)
		// we received a ctrl+c or other signal from the operating system
		case <-done:
			log.Println("Received os signal... canceling contexts and exiting gracefully")
			// send the signal to our "done" channel that we terminating.
			cancel()
			// this "blocks" until it receives a value from the channel,
			// meaning it will wait here until we have finished shutting down
			// gracefully. the goroutine running in the background writes to
			// this channel when it is done, so we know we need to sit here
			// waiting to receive again. we could range over this and expect to
			// receive N signals from N workers as well.
			<-done
			return
		}

	}
}
